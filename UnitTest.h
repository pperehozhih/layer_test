#ifndef UNITTEST_H
#define UNITTEST_H

#include "ADataHelper.h"
#include "Helper.hpp"

namespace tests {
    class UnitTest{
        a::ADataHelperReader<std::string, std::string> *m_reader;
    public:
        UnitTest(a::ADataHelperReader<std::string, std::string> *reader):m_reader(reader){}
        void Run();
    };
}

#endif // UNITTEST_H

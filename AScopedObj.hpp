#ifndef ASCOPEDOBJ_HPP
#define ASCOPEDOBJ_HPP

namespace a{
    template<typename Class_T>
    class AScopedObj{
        typedef void (Class_T::*F)(void);
        F m_end_func;
        Class_T *m_obj;
    public:
        AScopedObj(Class_T* obj, F start, F end): m_end_func(end), m_obj(obj){
            (m_obj->*start)();
        }
        AScopedObj(F start, F end): m_end_func(end), m_obj(0){
            (m_obj->*start)();
        }
        inline void SetInstance(Class_T* obj){m_obj = obj;}
        virtual ~AScopedObj(){
            (m_obj->*m_end_func)();
        }
    };
}

#endif // ASCOPEDOBJ_HPP

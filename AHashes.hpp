#ifndef AHASHES_HPP
#define AHASHES_HPP

#include <stdint.h>

namespace a {
    // Константа, взятая из SunriseDD.
    const size_t DD_SIGNIFICANT_CHARS = 32;

    // Функция хэширования, взятая из SunriseDD.
    //
    // --------------------------------------------------------------------------
    // Function:  dd_FNVM_hash_string(string)
    // --------------------------------------------------------------------------
    //
    // Returns the hash value of the null terminated C string <string>  using the
    // FNVM hash algorithm  (Fowler, Noll and Vo hash,  modified by Bret Mulvey).
    // The number of  significant characters  for  which the hash value  will  be
    // calculated   is   limited   to   the   value    returned    by    function
    // dd_significant_chars().
    //
    // Returns 0 if <string> is empty or NULL.
    //
    inline uint32_t
    hash_FNVM(const char *string, size_t string_size)
    {
       // seed values for FNV hashes
       const uint32_t FNVM_INIT32 = 0x811c9dc5;
       const uint32_t FNVM_PRIME32 = 0x01000193;

       register uint32_t index = 0, hash = FNVM_INIT32;

       if (!string_size)
          return 0;

       while ((index < string_size) && (index < DD_SIGNIFICANT_CHARS))
       {
          hash = hash ^ string[index];
          hash = hash * FNVM_PRIME32;
          index++;
       } // end while

       hash = hash + (hash << 13);
       hash = hash ^ (hash >> 7);
       hash = hash + (hash << 3);
       hash = hash ^ (hash >> 17);
       hash = hash + (hash << 5);

       return (hash & 0x7fffffff);
    } // end dd_FNV_hash_string
}

#endif // AHASHES_HPP

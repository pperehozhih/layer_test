#ifndef ATIMEDIFF_HPP
#define ATIMEDIFF_HPP

#include "AScopedObj.hpp"
#include <ctime>
#include <string>
#include <iostream>

#ifdef WIN32
#else
    #include <sys/time.h>
#endif

namespace a {
    class ADiffClock
    {
    protected:
        clock_t m_begin;
        clock_t m_diff;
    public:
        ADiffClock():m_diff(0) {Start();}
        inline void Start(){
            m_begin = clock();
        }
        inline void End(){
            m_diff = clock() - m_begin;
        }
        inline clock_t Diff(){return m_diff;}
    };

    class ADiffTime
    {
    protected:
        timeval m_begin;
        double m_diff;
    public:
        ADiffTime():m_diff(0) {Start();}
        inline void Start(){
            gettimeofday(&m_begin, 0);
        }

        inline double Current(){
            timeval end;
            gettimeofday(&end, 0);
            long seconds  = end.tv_sec  - m_begin.tv_sec;
            long useconds = end.tv_usec - m_begin.tv_usec;
            return seconds * 1000.0 + useconds / 1000.0;
        }

        inline void End(){
            m_diff = Current();
        }
        inline double Diff(){return m_diff;}
    };

    template<typename Class_Diff>
    class AElapsed : public Class_Diff{
        std::string m_tag;
    public:
        AElapsed(const std::string& tag):m_tag(tag){}
        inline operator bool () const{
            return Class_Diff::m_diff == 0;
        }
        virtual ~AElapsed(){
            std::cout << m_tag << " " << Class_Diff::Diff() << " ms" << std::endl;
        }
    };




#define A___elapsed_clock(M, T) for(a::AElapsed<a::ADiffClock> M##_elapsed_ = a::AElapsed<a::ADiffClock>(T); M##_elapsed_; M##_elapsed_.End())

#define A___elapsed_time(M, T) for(a::AElapsed<a::ADiffTime> M##_elapsed_ = a::AElapsed<a::ADiffTime>(T); M##_elapsed_; M##_elapsed_.End())

}

#endif // ATIMEDIFF_HPP

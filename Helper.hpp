#ifndef HELPER_HPP
#define HELPER_HPP

#include <sstream>
#include <string>
#include <cstdlib>

namespace helpers{
    namespace random_string {
        //(c) http://www.cplusplus.com/forum/windows/88843/
        static const char alphanum[] =
        "0123456789"
        "!@#$%^&*"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

        static int stringLength = sizeof(alphanum) - 1;

        inline char GenRandom()  // Random string generator function.
        {

            return alphanum[rand() % stringLength];
        }
        inline std::string GenRandomString(unsigned char length){
            std::stringstream str;
            for(unsigned char i = 0; i < length; ++i)
            {
                str.put(GenRandom());
            }
            return str.str();
        }
    }
}

#endif // HELPER_HPP

#ifndef ADATAHELPER_H
#define ADATAHELPER_H

#include "AThreading.h"
#include "ATimeDiff.hpp"
#include <vector>
#include <map>

namespace a {
    template<typename Class_Key,
             typename Class_Value>
    class ADataHelperReader{
    public:
        virtual Class_Value* GetValue(const Class_Key &key) = 0;
        virtual void StartBatch() = 0;
        virtual void FinishBatch() = 0;
        virtual void AddValue(const Class_Key &key, const Class_Value &value) = 0;
        virtual void UpdateValue(const Class_Key &key, const Class_Value &value) = 0;
    };

    template<typename Class_Value>
    struct ADataHelperContaner{
        Class_Value value;
        bool        locked;
        ADataHelperContaner(const Class_Value &value):value(value), locked(false){}
        ADataHelperContaner(const ADataHelperContaner &data):value(data.value), locked(data.locked){}
        void operator = (const ADataHelperContaner &data){
            value = data.value;
            locked = data.locked;
        }
    };

    class ADataHelperTimeOutException : public std::exception{
    };

    class ADataHelperOutRange : public std::exception{
    };

    class ADataHelperLogicError : public std::exception{
    };

    template<typename Class_Key,
             typename Class_Value,
             typename Class_Container = std::map<Class_Key, ADataHelperContaner<Class_Value> > >
    class ADataHelper
    {
        Class_Container                                                 m_container;
        Class_Value                                                     m_default_value;
        double                                                          m_timeout;
        double                                                          m_flush_time;
        ADataHelperReader<Class_Key, Class_Value>*                      m_reader;
        std::vector<Class_Key>                                          m_updates_key_in_db;
        std::vector<Class_Key>                                          m_add_key_in_db;
        AMutex                                                          m_container_lock;
        AMutex                                                          m_reader_lock;
        ADiffTime                                                       m_flushtimer;
    public:
        ADataHelper(ADataHelperReader<Class_Key, Class_Value> *reader, Class_Value default_value, long timeout)
            :m_default_value(default_value), m_reader(reader), m_timeout(timeout), m_flush_time(5000){
        }

        ~ADataHelper(){
            DirectFlush();
        }

        inline size_t Size(){
            size_t size = 0;
            A___sync(m_container_lock){
                size = m_container.size();
            }
            return size;
        }

        inline void Flush(){
            bool need_flush = false;
            A___sync(m_reader_lock){
                need_flush = m_flushtimer.Current() > m_flush_time;
            }
            if (need_flush)
                DirectFlush();
        }

        inline void DirectFlush(){
            A___sync(m_reader_lock){
                m_reader->StartBatch();
                for(typename std::vector<Class_Key>::iterator iter = m_add_key_in_db.begin();
                    m_add_key_in_db.end() != iter; iter++){
                    m_container_lock.lock();
                    typename Class_Container::iterator search = m_container.find(*iter);
                    if (m_container.end() == search){
                        throw ADataHelperLogicError();
                    }
                    m_reader->AddValue(*iter, search->second.value);
                    m_container_lock.unlock();
                }

                m_add_key_in_db.clear();

                for(typename std::vector<Class_Key>::iterator iter = m_updates_key_in_db.begin();
                    m_updates_key_in_db.end() != iter; iter++){
                    m_container_lock.lock();
                    typename Class_Container::iterator search = m_container.find(*iter);
                    if (m_container.end() == search){
                        throw ADataHelperLogicError();
                    }
                    m_reader->UpdateValue(*iter, search->second.value);
                    m_container_lock.unlock();
                }

                m_updates_key_in_db.clear();

                m_reader->FinishBatch();
                m_flushtimer.Start();
            }
        }

        const Class_Value& GetAndLockValue(const Class_Key &key){
            m_container_lock.lock();
            typename Class_Container::iterator search = m_container.find(key);
            if (m_container.end() == search){
                m_container_lock.unlock();
                A___sync(m_reader_lock){
                    Class_Value *value = m_reader->GetValue(key);
                    m_container_lock.lock();
                    if (value != 0){
                        m_container.insert(std::make_pair(key, ADataHelperContaner<Class_Value>(*value)));
                        delete value;
                    }else{
                        m_container.insert(std::make_pair(key, ADataHelperContaner<Class_Value>(m_default_value)));
                        m_add_key_in_db.push_back(key);
                    }
                }
                search = m_container.find(key);
            }
            ADiffTime started;
            while (search->second.locked) {
                m_container_lock.unlock();
                AProcessedTimeShared();
                if (started.Current() > m_timeout){
                    throw ADataHelperTimeOutException();
                }
                m_container_lock.lock();
            }
            search->second.locked = true;
            m_container_lock.unlock();
            Flush();
            return search->second.value;
        }

        void SetValue(const Class_Key &key, const Class_Value &value){
            A___sync(m_container_lock){
                typename Class_Container::iterator search = m_container.find(key);
                if (m_container.end() == search){
                    m_container.insert(std::make_pair(key, ADataHelperContaner<Class_Value>(value)));
                    m_container_lock.unlock();
                    A___sync(m_reader_lock){
                        m_add_key_in_db.push_back(key);
                    }
                    m_container_lock.lock();
                }else{
                    search->second.value = value;
                    m_container_lock.unlock();
                    A___sync(m_reader_lock){
                        m_updates_key_in_db.push_back(key);
                    }
                    m_container_lock.lock();
                }
            }
            Flush();
        }

        void UnLock(const Class_Key &key){
            m_container_lock.lock();
            typename Class_Container::iterator search = m_container.find(key);
            if (m_container.end() == search){
                m_container_lock.unlock();
                throw ADataHelperOutRange();
            }
            if (!search->second.locked){
                throw ADataHelperLogicError();
            }
            search->second.locked = false;
            m_container_lock.unlock();
            Flush();
        }
    };
}

#endif // ADATAHELPER_H

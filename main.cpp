#include <iostream>
#include <sstream>
#include "AHashes.hpp"
#include "ATimeDiff.hpp"
#include "ADataHelper.h"
#include "UnitTest.h"
#include "AThreading.h"
#include <sqlite3.h>
#include <cstdlib>

class SQLiteDataReader : public a::ADataHelperReader<std::string, std::string>{
    sqlite3 *m_db;
public:
    SQLiteDataReader(std::string base_name) : m_db(0){
        int rc = sqlite3_open(base_name.c_str(), &m_db);
        if (rc)
            throw std::exception();
        rc = sqlite3_exec(m_db, "CREATE TABLE test(k TEXT PRIMARY KEY, v TEXT);", 0, 0, 0);
        if (rc)
            throw std::exception();
    }

    ~SQLiteDataReader(){
        if (m_db)
            sqlite3_close(m_db);
    }

    virtual std::string* GetValue(const std::string &key){
        std::string sql = "select v from test where k = ?;";
        sqlite3_stmt *stmt;
        std::string* result = 0;
        int rc = sqlite3_prepare(m_db, sql.c_str(), sql.length(), &stmt, 0);
        if (rc != SQLITE_OK)
            throw std::exception();
        sqlite3_bind_text(stmt, 1, key.c_str(), key.length(), 0);
        if (sqlite3_step(stmt) == SQLITE_ROW){
            result = new std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
        }
        sqlite3_finalize(stmt);
        return result;
    }

    virtual void StartBatch(){
        sqlite3_exec(m_db, "BEGIN TRANSACTION;", 0, 0, 0);
    }

    virtual void FinishBatch(){
        sqlite3_exec(m_db, "END TRANSACTION;", 0, 0, 0);
    }

    virtual void AddValue(const std::string &key, const std::string &value){
        std::string sql = "insert into test(k, v) values (?,?);";
        sqlite3_stmt *stmt;
        int rc = sqlite3_prepare(m_db, sql.c_str(), sql.length(), &stmt, 0);
        if (rc)
            throw std::exception();
        sqlite3_bind_text(stmt, 1, key.c_str(), key.length(), 0);
        sqlite3_bind_text(stmt, 2, value.c_str(), value.length(), 0);
        sqlite3_step(stmt);
        sqlite3_finalize(stmt);
    }

    virtual void UpdateValue(const std::string &key, const std::string &value){
        std::string sql = "update test set v = ? where k = ?;";
        sqlite3_stmt *stmt;
        int rc = sqlite3_prepare(m_db, sql.c_str(), sql.length(), &stmt, 0);
        if (rc)
            throw std::exception();
        sqlite3_bind_text(stmt, 2, key.c_str(), key.length(), 0);
        sqlite3_bind_text(stmt, 1, value.c_str(), value.length(), 0);
        sqlite3_step(stmt);
        sqlite3_finalize(stmt);
    }
};

class DummyDataReader : public a::ADataHelperReader<std::string, std::string>{
public:
    virtual std::string* GetValue(const std::string &key){
        return 0;
    }

    virtual void StartBatch(){

    }

    virtual void FinishBatch(){

    }

    virtual void AddValue(const std::string &key, const std::string &value){
        //std::cout << "Add [" << key << "] => [" << value << "]" << std::endl;
    }

    virtual void UpdateValue(const std::string &key, const std::string &value){
        //std::cout << "Update [" << key << "] => [" << value << "]" << std::endl;
    }
};

int main()
{
    srand(time(0));
    {//Dummy
        A___elapsed_time(dummy_timer, "Dummy Test finish "){
            DummyDataReader reader;
            tests::UnitTest ut(&reader);
            ut.Run();
        }
    }{//SQLite
        unlink("test.db");
        A___elapsed_time(dummy_timer, "SQLite Test finish "){
            SQLiteDataReader reader("test.db");
            tests::UnitTest ut(&reader);
            ut.Run();
        }
    }
    return 0;
}


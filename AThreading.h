#ifndef ATHREADING_H
#define ATHREADING_H

#include "AScopedObj.hpp"
#include "AConfig.h"
#ifdef __CPLUSPLUS_11_SUPPORT
#include <mutex>
#include <thread>
#endif

namespace a {
    void AProcessedTimeShared();
#ifndef __CPLUSPLUS_11_SUPPORT
    struct IFunctorData{
        virtual void operator()() = 0;
        virtual ~IFunctorData(){}
    };

    template<typename Functor,typename Arg1, typename Arg2, typename Arg3>
    struct IFunctorDataArg3 : public IFunctorData{
        Arg1 a1;
        Arg2 a2;
        Arg3 a3;
        Functor func;
        IFunctorDataArg3(Functor func, Arg1 a1, Arg2 a2, Arg3 a3):func(func), a1(a1), a2(a2), a3(a3){}
        virtual void operator()(){
            func(a1,a2,a3);
        }
    };

    template<typename Functor,typename Arg1, typename Arg2>
    struct IFunctorDataArg2 : public IFunctorData{
        Arg1 a1;
        Arg2 a2;
        Functor func;
        IFunctorDataArg2(Functor func, Arg1 a1, Arg2 a2):func(func), a1(a1), a2(a2){}
        virtual void operator()(){
            func(a1, a2);
        }
    };

    template<typename Functor,typename Arg1>
    struct IFunctorDataArg1 : public IFunctorData{
        Arg1 a1;
        Functor func;
        IFunctorDataArg1(Functor func, Arg1 a1):func(func), a1(a1){}
        virtual void operator()(){
            func(a1);
        }
    };

    template<typename Functor>
    struct IFunctorDataArg0 : public IFunctorData{
        Functor func;
        IFunctorDataArg0(Functor func):func(func){}
        virtual void operator()(){func();}
    };

    struct AThreadData;
    class AThread
    {
        AThreadData*     m_data;
        IFunctorData*    m_functor;
        void Init();
    public:
        template<typename Function, typename Arg1, typename Arg2, typename Arg3>
        AThread(Function func, Arg1 a1, Arg2 a2, Arg3 a3){
            m_functor = new IFunctorDataArg3<Function, Arg1, Arg2, Arg3>(func, a1, a2, a3);
            Init();
        }

        template<typename Function, typename Arg1, typename Arg2>
        AThread(Function func, Arg1 a1, Arg2 a2){
            m_functor = new IFunctorDataArg2<Function, Arg1, Arg2>(func, a1, a2);
            Init();
        }

        template<typename Function, typename Arg1>
        AThread(Function func, Arg1 a1){
            m_functor = new IFunctorDataArg1<Function, Arg1>(func, a1);
            Init();
        }

        template<typename Function>
        AThread(Function func){
            m_functor = new IFunctorDataArg0<Function>(func);
            Init();
        }

        void Run();

        void detach();
        void join();
        virtual ~AThread();
    };

    struct AMutexData;
    class AMutex
    {
        AMutexData* m_data;
    public:
        AMutex();
        virtual ~AMutex();
        void lock();
        void unlock();
        bool try_lock();
    };
#else
    typedef std::mutex AMutex;
    typedef std::thread AThread;
#endif

    class AScopedMutex : public AScopedObj<AMutex>{
    protected:
        bool m_first_check;
    public:
        AScopedMutex(AMutex &mutex):AScopedObj<AMutex>(&mutex, &AMutex::lock, &AMutex::unlock), m_first_check(true){}
        inline operator bool () const{
            return m_first_check;
        }
        inline void SetNotFirstTrue(){
            m_first_check = false;
        }
    };

    #define A___sync(M) for(a::AScopedMutex M##_lock_ = a::AScopedMutex(M); M##_lock_; M##_lock_.SetNotFirstTrue())
}

#endif // ATHREADING_H

#include "AThreading.h"

#ifndef WIN32
#include <sched.h>
#include <pthread.h>
#else
#include <windows.h>
#endif

#include <stdlib.h>

namespace a {
    void AProcessedTimeShared(){
//(c) http://stackoverflow.com/questions/6504405/how-do-i-obtain-sleep0-like-behaviour-in-linux
#ifndef WIN32
        sched_yield();
#else
        Sleep(0);
#endif
    }

#ifndef WIN32

    struct AThreadData{
        pthread_t       pthread;
        pthread_attr_t  pthread_attr;
        AThreadData(){
            pthread_attr_init(&pthread_attr);
        }
        ~AThreadData(){
            pthread_attr_destroy(&pthread_attr);
        }
    };

    struct AMutexData{
        pthread_mutex_t     mutex;
        pthread_mutexattr_t mutex_attr;
        AMutexData(){
            pthread_mutexattr_init(&mutex_attr);
            pthread_mutex_init(&mutex, 0);
        }
        ~AMutexData(){
            pthread_mutex_destroy(&mutex);
            pthread_mutexattr_destroy(&mutex_attr);
        }
    };

    void* ThreadRunner(void* data){
        AThread* thread = reinterpret_cast<AThread*>(data);
        thread->Run();
        return 0;
    }

    //PThreads
    void AThread::Init(){
        m_data = new AThreadData();
        pthread_create(&m_data->pthread, &m_data->pthread_attr, ThreadRunner, this);
    }

    void AThread::Run(){
        if (m_functor)
            (*m_functor)();
    }

    void AThread::join(){
        if (m_data)
            pthread_join(m_data->pthread, 0);
    }

    void AThread::detach(){
        if (m_data)
            pthread_detach(m_data->pthread);
    }

    AThread::~AThread(){
        if (m_data){
            delete m_data;
        }
        if (m_functor){
            delete m_functor;
        }
    }

    //Mutex
    AMutex::AMutex(){
        m_data = new AMutexData();
    }

    void AMutex::lock(){
        pthread_mutex_lock(&m_data->mutex);
    }

    void AMutex::unlock(){
        pthread_mutex_unlock(&m_data->mutex);
    }

    bool AMutex::try_lock(){
        return pthread_mutex_trylock(&m_data->mutex);
    }

    AMutex::~AMutex(){
        delete m_data;
    }

#else
#endif

}

#include "UnitTest.h"
#include <iostream>

namespace tests {
    const std::string default_value = "";
    const int random_string_len = 15;
    inline void EchoResult(bool value){
        if (value){
            std::cout << "Success" << std::endl;
        }else{
            std::cout << "Fail" << std::endl;
        }
    }

    void ThreadedFunction(a::ADataHelper<std::string, std::string>* helper){
        A___elapsed_time(timer, "100000 added"){
            for(int i = 0; i < 100000; i++){
                std::string _ = helpers::random_string::GenRandomString(random_string_len);
                std::string __ = helpers::random_string::GenRandomString(random_string_len);
                helper->SetValue(_, __);
                if (__ != helper->GetAndLockValue(_)){
                    throw std::exception();
                }
                helper->UnLock(_);
            }
        }
    }

    void ThreadedFunction2(a::ADataHelper<std::string, std::string>* helper){
        A___elapsed_time(timer, "100000 get"){
            for(int i = 0; i < 100000; i++){
                std::string _ = helpers::random_string::GenRandomString(random_string_len);
                helper->GetAndLockValue(_);
                helper->UnLock(_);
            }
        }
    }

    void UnitTest::Run(){
        bool result = false;
        a::ADataHelper<std::string, std::string> helper(m_reader, default_value, 1200);
        std::string _ = helpers::random_string::GenRandomString(random_string_len);
        std::cout << "Begin simple read and lock [" << _ <<"]: ";
        std::string __ = helper.GetAndLockValue(_);
        EchoResult(__ == default_value);

        try{
            result = false;
            std::cout << "Begin simple unlock [" << _ <<"]: ";
            helper.UnLock(_);
            result = true;
        }catch(std::exception ex){
        }

        EchoResult(result);

        __ = helpers::random_string::GenRandomString(random_string_len);
        std::cout << "Begin value set [" << _ <<"]:[" << __ << "]: ";
        helper.SetValue(_, __);
        std::string ___ = helper.GetAndLockValue(_);
        EchoResult(__ == ___);
        helper.SetValue(_, ___);

        try{
            result = true;
            std::cout << "Begin double lock [" << _ <<"]: ";
            helper.GetAndLockValue(_);
            result = false;
        }catch(std::exception ex){
        }
        EchoResult(result);
        helper.UnLock(_);

        std::cout << "Begin different value get [" << _ <<"]:[" << __ << "]: ";
        try{
            result = false;
            helper.GetAndLockValue(_);
            helper.GetAndLockValue(__);
            helper.UnLock(__);
            helper.UnLock(_);
            result = true;
        }catch(std::exception ex){
        }
        EchoResult(result);

        for (int i = 0; i < 7; i++){
            std::cout << "Iteration MIX " << i << " Start" << std::endl;
            a::AThread th1(ThreadedFunction, &helper);
            a::AThread th2(ThreadedFunction2, &helper);
            th1.join();
            th2.join();
            std::cout << "Iteration MIX " << i << " Finish [Count ->" << helper.Size() << "]" << std::endl;
        }

        for (int i = 0; i < 7; i++){
            std::cout << "Iteration GET " << i << " Start" << std::endl;
            a::AThread th1(ThreadedFunction2, &helper);
            a::AThread th2(ThreadedFunction2, &helper);
            th1.join();
            th2.join();
            std::cout << "Iteration GET " << i << " Finish [Count ->" << helper.Size() << "]" << std::endl;
        }

        for (int i = 0; i < 7; i++){
            std::cout << "Iteration SET " << i << " Start" << std::endl;
            a::AThread th1(ThreadedFunction2, &helper);
            a::AThread th2(ThreadedFunction2, &helper);
            th1.join();
            th2.join();
            std::cout << "Iteration SET " << i << " Finish [Count ->" << helper.Size() << "]" << std::endl;
        }
    }
}
